vim.g.nvim_tree_auto_open = 0
vim.g.nvim_tree_auto_close = 1
vim.g.nvim_tree_lsp_diagnostics = 1
vim.g.nvim_tree_tab_open = 1
vim.g.nvim_tree_show_icons = {
    git = 0,
    folders = 1,
    files = 1,
    folder_arrow = 0
}

vim.api.nvim_set_keymap('n', '<S-TAB>', ':call ToggleTree()<CR>', {noremap = true, silent = true})

tree = {
    open = false
}

function ToggleTree()
    if (tree.open) then
	tree.open = false
	require'nvim-tree'.close()
	require'bufferline.state'.set_offset(0)
    else
	tree.open = true
	require'nvim-tree'.open()
	require'bufferline.state'.set_offset(30, 'Explorer')
    end
end

vim.cmd([[
function OpenTree()
    lua require'nvim-tree'.open()
    lua require'bufferline.state'.set_offset(31, 'Explorer')
    wincmd p
endfunction

function ToggleTree()
    lua ToggleTree()
endfunction
]])
