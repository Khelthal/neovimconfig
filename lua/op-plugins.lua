return require('packer').startup(function()
  use { 'wbthomason/packer.nvim' }

  use { 'windwp/nvim-autopairs' }

  use { 'akinsho/nvim-toggleterm.lua' }

  use { 'andymass/vim-matchup', event = 'VimEnter' }

  use { 'christianchiarulli/nvcode-color-schemes.vim' }

  use { 'hoob3rt/lualine.nvim' }

  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }

  use { 'kyazdani42/nvim-web-devicons' }

  use { 'kyazdani42/nvim-tree.lua' }

  use { 'romgrk/barbar.nvim' }

  use { 'zah/nim.vim' }

  use { 'neovim/nvim-lspconfig', 'kabouzeid/nvim-lspinstall' }

  use { 'hrsh7th/nvim-compe', 'ray-x/lsp_signature.nvim' }

  use { 'hrsh7th/vim-vsnip', 'hrsh7th/vim-vsnip-integ' }

  use { 'ahmedkhalf/lsp-rooter.nvim' }

  use {
  'nvim-telescope/telescope.nvim',
  requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}}
  }

  use { 'glepnir/dashboard-nvim' }

  use { 'lukas-reineke/indent-blankline.nvim' }

  use { 'tpope/vim-commentary' }
end)
