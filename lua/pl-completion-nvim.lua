vim.cmd([[
" Use <Tab> and <S-Tab> to navigate through popup menu
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Set completeopt to have a better completion experience
set completeopt=menuone,noinsert,noselect

" Avoid showing message extra message when using completion
set shortmess+=c

let g:completion_matching_smart_case = 1

"map <c-p> to manually trigger completion
imap <silent> <c-p> <Plug>(completion_trigger)

let g:completion_trigger_character = ['.', '::']
]])
