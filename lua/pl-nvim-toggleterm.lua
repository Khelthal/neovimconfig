require("toggleterm").setup{
  -- size can be a number or function which is passed the current terminal
  size = 20,
  open_mapping = [[<C-SPACE>]],
  hide_numbers = true, -- hide the number column in toggleterm buffers
  shade_terminals = true,
  shading_factor = 1, -- the degree by which to darken to terminal colour, default: 1 for dark backgrounds, 3 for light
  start_in_insert = true,
  insert_mappings = true, -- whether or not the open mapping applies in insert mode
  persist_size = true,
  direction = 'float',
  close_on_exit = true
}

vim.api.nvim_set_keymap('n', '<F5>', ':call v:lua.execute_code()<CR>', {noremap = true, silent = true})

function _G.execute_code()
  local projpath = vim.api.nvim_eval("getcwd()")
  local proj_command = get_project_command()

  if proj_command == ''
  then
    local filepath = vim.api.nvim_eval([[ expand("%:p:h") ]])
    local filename = vim.api.nvim_eval([[ expand("%:t:r") ]])
    local fileexte = vim.api.nvim_eval([[ expand("%:t:e") ]])
    local filetype = vim.bo.filetype

    local run_commands =
    {
      ["python"] = "python " .. filepath .. "/" .. filename .. "." .. fileexte,
      ["nim"]    = "nim c -r " .. filepath .. "/" .. filename .. "." .. fileexte,
      ["cpp"]    = "g++ " .. filepath .. "/" .. filename .. "." .. fileexte .. " -o " .. filepath .. "/" .. filename .. " && " .. filepath .. "/" .. filename,
      ["cs"]    = "dotnet run " .. projpath .. "/",
    }

    vim.cmd("TermExec cmd='" .. run_commands[filetype] .. "'")
  else
    vim.cmd("TermExec cmd='" .. proj_command .. "'")
  end
end

function get_project_command()
  local projpath = vim.api.nvim_eval("getcwd()")
  local filetype = vim.bo.filetype
  local proj_command = ""

  --if vim.api.nvim_eval("filereadable(\"" .. projpath .. "/project.godot\")")
  --then
  --  proj_command = "nake build"
  --end
  return proj_command
end
