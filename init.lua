require('op-plugins')

vim.cmd([[
source /home/elias/.config/nvim/vimscript/togglecompletion.vim
syntax on
colorscheme aurora

if (has("termguicolors"))
    set termguicolors
    hi LineNr ctermbg=NONE guibg=NONE
endif
]])
	
vim.g.nvcode_termcolors=256
vim.bo.ts=2
vim.bo.sw=2
vim.bo.expandtab=true

vim.api.nvim_set_keymap('v', '<', '<gv', {noremap = true, silent = true})
vim.api.nvim_set_keymap('v', '>', '>gv', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<C-e>', ':BufferClose<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<S-Left>', ':BufferPrevious<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<S-Right>', ':BufferNext<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<C-F>', ':Telescope find_files<CR>', {noremap = true, silent = true})

vim.bo.smartindent = true
--vim.wo.number = true
--vim.wo.relativenumber = true
--vim.wo.cursorline = true
vim.o.hidden = true
vim.wo.wrap = false
vim.o.pumheight = 10
vim.o.showtabline = 2

require'lsp_signature'.on_attach()
require('lualine').setup{
    options = {
        theme = 'nord'
    }
}
require("lsp-rooter").setup{}
require('pl-nvim-tree')
require('pl-nvim-toggleterm')
require('pl-nvim-treesitter')
require('pl-nvim-autopairs')
require('pl-nvim-lspconfig')
require('pl-nvim-compe')
require('pl-dashboard-nvim')
