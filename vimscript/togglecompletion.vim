function! togglecompletion#confirm(...) abort
  let l:completeopts = split(&completeopt, ',')
  for l:opt in ['menuone', 'noselect']
    if index(l:completeopts, l:opt) == -1
      echohl ErrorMsg
      echomsg '[nvim-compe] You must set `set completeopt=menuone,noselect` in your vimrc.'
      echohl None
    endif
  endfor

  let l:option = s:normalize(get(a:000, 0, {}))
  let l:index = complete_info(['selected']).selected
  let l:select = get(l:option, 'select', v:false)
  let l:selected = l:index != -1
  if mode()[0] ==# 'i' && pumvisible() && (l:select || l:selected)
    if &filetype == "nim"
      call feedkeys("\<Plug>(compe-confirm)", '')
      return s:fallback(l:option)
    endif
    let l:info = luaeval('require"compe"._confirm_pre(_A)', (l:selected ? l:index + 1 : 1))
    if !empty(l:info)
      call feedkeys(repeat("\<BS>", strchars(getline('.')[l:info.offset - 1 : col('.') - 2], 1)), 'n')
      call feedkeys(l:info.item.word, 'n')
      call feedkeys("\<Plug>(compe-confirm)", '')
      return "\<Ignore>"
    endif
  endif
  return s:fallback(l:option)
endfunction

function! s:normalize(option) abort
  if type(a:option) == v:t_string
    return { 'keys': a:option, 'mode': 'n' }
  endif
  return a:option
endfunction

function! s:fallback(option) abort
  if has_key(a:option, 'keys') && get(a:option, 'mode', 'n') !=# 'n'
    call feedkeys(a:option.keys, a:option.mode)
    return "\<Ignore>"
  endif
  return get(a:option, 'keys', "\<Ignore>")
endfunction
